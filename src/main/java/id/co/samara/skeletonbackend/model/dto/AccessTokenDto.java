package id.co.samara.skeletonbackend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccessTokenDto {

    private String accessToken;
    private long expiresIn;
    private String tokenType;

}
