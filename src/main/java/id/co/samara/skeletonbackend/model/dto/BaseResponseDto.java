package id.co.samara.skeletonbackend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class BaseResponseDto {

    private final String responseCode;
    private final String responseMessage;

    public BaseResponseDto() {
        this.responseCode = "00";
        this.responseMessage = "SUCCESS";
    }

}
