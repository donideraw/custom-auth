package id.co.samara.skeletonbackend.configuration;

import id.co.samara.skeletonbackend.configuration.exception.UnauthorizedException;
import id.co.samara.skeletonbackend.service.AuthService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import static id.co.samara.skeletonbackend.configuration.ConstantVariable.BEARER;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Slf4j
public class WebInterceptor implements HandlerInterceptor {

    private final AuthService authService;

    public WebInterceptor(AuthService authService) {
        this.authService = authService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        try {
            String authHeader = request.getHeader(AUTHORIZATION);

            if (authHeader == null || !authHeader.startsWith(BEARER)) {
                throw new UnauthorizedException();
            }

            validateToken(authHeader.substring(7));

            return true;
        } catch (Exception e) {
            throw new UnauthorizedException("99", e.getMessage());
        }

    }

    private void validateToken(String token) {
        authService.authenticate(token);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        log.info("postHandle");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        log.info("afterCompletion");
    }
}
