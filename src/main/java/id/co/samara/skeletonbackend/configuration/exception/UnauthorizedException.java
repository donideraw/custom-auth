package id.co.samara.skeletonbackend.configuration.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UnauthorizedException extends RuntimeException {

    private final String responseCode;
    private final String responseMessage;

    public UnauthorizedException() {
        this.responseCode = "01";
        this.responseMessage = "Unauthorized";
    }

    public UnauthorizedException(String responseCode, String responseMessage) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }
}
