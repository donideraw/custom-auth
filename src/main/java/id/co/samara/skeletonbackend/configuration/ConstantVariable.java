package id.co.samara.skeletonbackend.configuration;

public final class ConstantVariable {

    private ConstantVariable() {
        // constructor
    }

    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String BEARER = "Bearer";



    // Cache Name
    public static final String JWT_CACHE_NAME = "jwtCache";
    public static final String USER_CACHE_NAME = "userCache";



}
