package id.co.samara.skeletonbackend.service;

import id.co.samara.skeletonbackend.configuration.exception.UnauthorizedException;
import id.co.samara.skeletonbackend.model.dto.AccessTokenDto;
import id.co.samara.skeletonbackend.model.dto.LoginDto;
import id.co.samara.skeletonbackend.utils.CacheUtil;
import id.co.samara.skeletonbackend.utils.JWTUtil;
import org.springframework.stereotype.Service;

import static id.co.samara.skeletonbackend.configuration.ConstantVariable.USER_CACHE_NAME;

@Service
public class AuthService {

    private final JWTUtil jwtUtil;

    public AuthService(JWTUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    public AccessTokenDto login(LoginDto loginDto) {

        // Validate password
        String password = (String) CacheUtil.getData(loginDto.getUsername(), USER_CACHE_NAME);
        if (password == null || !password.equals(loginDto.getPassword())) {
            throw new UnauthorizedException("02", "Invalid Username or Password");
        }

        return jwtUtil.generateToken(loginDto.getUsername());
    }

    public void authenticate(String token) {
        jwtUtil.validateToken(token);
    }
}
