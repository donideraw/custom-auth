package id.co.samara.skeletonbackend;

import id.co.samara.skeletonbackend.utils.CacheUtil;
import jakarta.annotation.PostConstruct;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkeletonBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkeletonBackendApplication.class, args);
	}

	@PostConstruct
	void injectData() {
		CacheUtil.putData("samara", "sadira", "userCache");
	}

}
