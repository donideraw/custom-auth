package id.co.samara.skeletonbackend.utils;

import java.util.HashMap;
import java.util.Map;

public class CacheUtil {

    private CacheUtil() {
        // Default Constructor
    }

    private static final Map<String, Map<String, Object>> localMap = new HashMap<>();

    public static void putData(String key, Object value, String cacheName) {
        Map<String, Object> map = localMap.computeIfAbsent(cacheName, k -> new HashMap<>());
        map.put(key, value);
    }

    public static Object getData(String key, String cacheName) {
        Map<String, Object> map = localMap.get(cacheName);
        if (map == null) {
            return null;
        }
        return map.get(key);
    }

    public static void removeData(String key, String cacheName) {
        Map<String, Object> map = localMap.get(cacheName);
        if (map != null) {
            map.remove(key);
        }
    }

}
