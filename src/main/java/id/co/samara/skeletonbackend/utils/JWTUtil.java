package id.co.samara.skeletonbackend.utils;

import id.co.samara.skeletonbackend.configuration.exception.UnauthorizedException;
import id.co.samara.skeletonbackend.model.dto.AccessTokenDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static id.co.samara.skeletonbackend.configuration.ConstantVariable.JWT_CACHE_NAME;

@Component
public class JWTUtil {


    @Value("${jwt.token.secret}")
    private String tokenSecret;

    @Value("${jwt.token.validity}")
    private long tokenValidity;

    public AccessTokenDto generateToken(String username) {
        AccessTokenDto accessToken = (AccessTokenDto) CacheUtil.getData(username, JWT_CACHE_NAME);

        if (accessToken == null) {
            Map<String, Object> claims = new HashMap<>();
            accessToken = createToken(claims, username);

            CacheUtil.putData(username, accessToken, JWT_CACHE_NAME);
        }

        return accessToken;
    }

    private AccessTokenDto createToken(Map<String, Object> claims, String username) {
        long expirationTime = System.currentTimeMillis() + tokenValidity * 1000;

        String token = Jwts.builder()
                .setClaims(claims)
                .setSubject(username)
                .setIssuer("skeletonBackendService")
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(expirationTime))
                .signWith(getSignKey(), SignatureAlgorithm.HS256).compact();

        return new AccessTokenDto(token, tokenValidity, "Bearer");
    }

    private Key getSignKey() {
        byte[] keyBytes = Decoders.BASE64.decode(tokenSecret);
        return Keys.hmacShaKeyFor(keyBytes);
    }

    public void validateToken(String token) {
        String username = extractUsername(token);
        AccessTokenDto accessToken = (AccessTokenDto) CacheUtil.getData(username, JWT_CACHE_NAME);

        if (accessToken == null || !accessToken.getAccessToken().equals(token)) {
           throw new UnauthorizedException("03", "Invalid Token");
        }

    }

    private String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    private  <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSignKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

}
